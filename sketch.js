var width = window.innerWidth
var height = window.innerHeight

// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

// angle between y-axis and the vector that spans [p1, p2]
let angle = (p1, p2) => Math.atan2(p2[1] - p1[1], p2[0] - p1[0])

// move a point or a triangle by v
let move_thing = (xys, v) => xys.map((e, i) => e + (i%2==0 ? v[0] : v[1]))

// rotate a point or a triangle around point c by theta
function rotate_thing(yys, c, th) {
  let at_origin = move_thing(yys, [-c[0], -c[1]])
  let rotated = at_origin.map((e, i, l) =>
    i%2 == 0
    ? e * cos(th) - l[i+1] * sin(th)  // ys
    : e * cos(th) + l[i-1] * sin(th)  // ys
  )
  return move_thing(rotated, c)
}

// ...I miss python
let range = ((n) => [...Array(n).keys()])

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
}


// yes this code is poop but the circle is pretty so ⚫
const swiggleSpeed = 0.05
function swiggle(p, c, t) {
  let segLen = 10
  let wiggleAmount = 1.8
  let thetaPC = angle(p, c)

  let wiggleA = wiggleAmount * (noise(p[0], p[1], p[0] + t * swiggleSpeed) - 0.5)
  let wiggleB = wiggleAmount * (noise(p[0], p[1], p[1] + t * swiggleSpeed) - 0.5)

  let a = [p[0] + (segLen * cos(wiggleA + thetaPC)), p[1] + (segLen * sin(wiggleA + thetaPC))]
  let b = [p[0] - (segLen * cos(wiggleB + thetaPC)), p[1] - (segLen * sin(wiggleB + thetaPC))]

  line(...p, ...a)
  line(...p, ...b)
}

var t = 0
const appearSpeed = 0.0005


function moveTowards(src, dest, distance) {
  distvect = move_thing(dest, [-src[0], -src[1]])
  normalized_transvect = [(distvect[0] / dist(src, dest)),(distvect[1] / dist(src, dest))]
  transvect = move_thing(distvect, [-1*normalized_transvect[0] * distance, -1*normalized_transvect[1] * distance])
  return move_thing(src, transvect)
}

segment_length = 50
let zero_curve = [100,100,101,101,402,402,403,403]
var last_curves = [zero_curve,zero_curve,zero_curve,zero_curve,zero_curve,zero_curve,zero_curve,zero_curve]
let last_length = 8
var current_array = zero_curve
var finisharr = [0,0,0,0,0,0,0,0]
let huehue = 180
function draw() {
  background(huehue, 80, 255)
  noFill();
  stroke(huehue, 80, 50);
  strokeWeight(100)

  let array = current_array
  let pp1 = moveTowards([array[2], array[3]], [last_curves[7-0][0], last_curves[7-0][1]], segment_length)
  let pp2 = moveTowards([array[4], array[5]], [last_curves[7-0][2], last_curves[7-0][3]], segment_length)
  let pp3 = moveTowards([array[6], array[7]], [last_curves[7-0][4], last_curves[7-0][5]], segment_length)
  finisharr = [mouseX,mouseY,pp1[0],pp1[1],pp2[0],pp2[1],pp3[0],pp3[1]]
  bezier(...finisharr)
  let yo = last_curves.shift()
  last_curves.push(finisharr)
  console.log(JSON.stringify(last_curves))
  current_array = finisharr

  // print smiley
  stroke(huehue, 80, 255)
  strokeWeight(10)
  bezier(mouseX - 30, mouseY + 10, mouseX - 25, mouseY +40, mouseX+25, mouseY+40, mouseX + 30, mouseY+10)
  circle(mouseX - 22, mouseY - 15, 5)
  circle(mouseX + 22, mouseY - 15, 5)

  t += 0.01
  // background(34, 15, 250)
  // let centerP = [width / 2, height / 2]
  // swiggle([300, 300], centerP, t)
  //
  // strokeWeight(1.0)
  // stroke(158, 15, 80)
  // let maxR = min([width / 2, height / 2])
  // for (var R = 0; R < maxR; R += 10) {
  //   let vC = R
  //   let threshold = (Math.cos(vC * 0.03 + (t * 0.4)) * 0.5) + 0.5
  //
  //   for (var th = 0; th < 2 * PI; th += 0.01) {
  //     let r = (R + noise(t + th) * 5)
  //     let theta = th + noise(t + th)
  //     let here = [centerP[0] + (cos(theta) * r),
  //                 centerP[1] + (sin(theta) * r)]
  //
  //     // let vC = dist(here, centerP)
  //     let appear = noise(here[0], here[1], t * appearSpeed)
  //     if (appear < threshold) {
  //       swiggle(here, centerP, t)
  //     }
  //   }
  // }

}
